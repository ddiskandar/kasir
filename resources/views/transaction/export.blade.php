<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Kasir</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

    <table width="100%">
        <tr>
            <td valign="top"></td>
            <td align="right">
                <h3>Toko Berkah Bintang</h3>
                <pre>
                    Sehat untuk Ibadah
                    Jalan Suryakencana No. 34 Cisaat
                    085624028940
                </pre>
            </td>
        </tr>

    </table>

        <table>
                <tr>
                    <td>Transaksi #{{ $transaction->id }}</td>
                </tr>
                <tr>
                    <td>Waktu Transaksi : {{ $transaction->created_at->isoFormat('dddd, DD MMMM gggg hh:mm') }}</td>
                </tr>
        </table>

    <br/>

    <table width="100%">
        <thead style="background-color: lightgray;">
        <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Harga Satuan</th>
            <th>Jumlah</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($transaction->products as $product)
            <tr>
                <td scope="row" align="center">
                    {{ $loop->iteration }}
                </td>
                <td>
                    {{ $product->nama_barang }}
                </td>
                <td align="center">
                    {{ $product->pivot->jumlah }}
                </td>
                <td align="right">
                    {{ rupiah($product->harga_satuan) }}
                </td>
                <td align="right">
                    {{ rupiah($product->pivot->harga_satuan) }}
                </td>
            </tr>
            @endforeach
        </tbody>

        <tfoot>
            <tr>
                <td colspan="3"></td>
                <td align="right">Total Harga</td>
                <td align="right" class="gray">{{ rupiah($transaction->total_harga) }}</td>
            </tr>
        </tfoot>
    </table>

</body>
</html>

