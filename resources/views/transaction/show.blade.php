@extends('layouts.master')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Transaksi #{{ $transaction->id }}</h1>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <p>Waktu Transaksi : {{ $transaction->created_at->isoFormat('dddd, DD MMMM gggg hh:mm') }}</p>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th class="text-center" scope="col">Jumlah</th>
                            <th class="text-right" scope="col">Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($transaction->products as $product)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $product->nama_barang }}</td>
                                <td class="text-center">{{ $product->pivot->jumlah }}</td>
                                <td class="text-right">{{ rupiah($product->pivot->harga_satuan) }}</td>
                            </tr>
                           @endforeach
                           <tr>
                            <td class="font-weight-bold text-right" colspan="3">Total Harga</td>
                            <td class="font-weight-bold text-right">{{ rupiah($transaction->total_harga) }}</td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
