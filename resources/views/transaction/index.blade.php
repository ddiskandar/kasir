@extends('layouts.master')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Daftar Produk</h1>

    @include('flash::message')

    <form method="GET" action="/transaction">
        <div class="form-row d-flex align-content-end">
            @if (auth()->user()->isAdmin())
            <div class="col-md-3 mb-3">
                <label for="tanggal">Tanggal</label>
                <input name="tanggal" value="{{ request('tanggal') }}" type="date" class="form-control" id="tanggal">
            </div>
            @endif
            <div class="col-md-4 mb-3">
                <label for="nama_barang">Nama Produk</label>
                <input name="nama_barang" value="{{ request('nama_barang') }}" type="text" class="form-control" id="nama_barang">
            </div>
            <div class="col-md-3 mb-3">
                <label for="total_harga">Total Harga</label>
                <input name="total_harga" value="{{ request('total_harga') }}" type="number" class="form-control" id="total_harga">
            </div>
            <div class="col-md-1 mb-3">
                <label for="operator">Operator</label>
                <select name="operator" class="custom-select" id="operator">
                    <option {{ request('operator') == '=' ? 'selected' : '' }} value='='>=</option>
                    <option {{ request('operator') == '>=' ? 'selected' : '' }} value=">=">>=</option>
                    <option {{ request('operator') == '<=' ? 'selected' : '' }} value="<="><=</option>
                </select>
            </div>
            <div class="col-md-1 mb-3">
                <label for="operator">&nbsp;</label>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar Pengguna</h6>
                    <div class="">
                        <a class="btn btn-success btn-circle btn-sm" href="{{ route('transaction.create') }}" role="button">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Waktu Transaksi</th>
                                    <th>Produk</th>
                                    <th>Total Harga</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Waktu Transaksi</th>
                                    <th>Produk</th>
                                    <th>Total Harga</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @forelse ($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->id }}</td>
                                    <td>{{ $transaction->created_at->isoFormat('dddd, DD MMMM gggg hh:mm') }}</td>
                                    <td>
                                        @foreach ($transaction->products as $product)
                                            <span>{{ $product->nama_barang }}{{ ! $loop->last ? ', ' : '' }}</span>
                                        @endforeach
                                    </td>
                                    <td>{{ rupiah($transaction->total_harga) }}</td>
                                    <td class="d-flex justify-content-center">
                                        <a class="btn btn-warning btn-circle btn-sm" href="{{ route('transaction.show', $transaction->id) }}" role="button">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a class="btn btn-info btn-circle btn-sm ml-1" href="{{ route('transaction.export', $transaction->id) }}" role="button">
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                        <form action="{{ route('transaction.destroy', $transaction->id) }}}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger ml-1 btn-circle btn-sm"
                                                onclick="return confirm('Yakin mau dihapus?');"
                                            >
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="5">Tidak ada data yang ditemukan</td>
                                </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('styles')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                columnDefs: [
                    {
                        targets: 3,
                        className: 'text-right'
                    },
                    {
                        targets: 4,
                        className: 'text-center'
                    }
                ],
                'searching': false,
                'order':[
                    [0,'DESC']
                ]
                });
        });
    </script>
@endpush
