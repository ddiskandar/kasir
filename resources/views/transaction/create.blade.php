@extends('layouts.master')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Transaksi Baru</h1>

    @livewire('add-new-transaction')

@endsection
