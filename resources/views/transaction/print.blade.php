<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Receipt example</title>
        <style>
            * {
                font-size: 12px;
                font-family: 'Times New Roman';
            }

            td,
            th,
            tr,
            table {
                border-top: 1px solid black;
                border-collapse: collapse;
            }

            td.description,
            th.description {
                width: 110px;
                max-width: 110px;
            }

            td.quantity,
            th.quantity {
                width: 20px;
                max-width: 20px;
                word-break: break-all;
            }

            td.price,
            th.price {
                width: 100px;
                max-width: 100px;
                word-break: break-all;
            }

            .centered {
                text-align: center;
                align-content: center;
            }

            .right {
                text-align: right;
                align-content: right;
            }

            .ticket {
                width: 155px;
                max-width: 155px;
            }

            @media print {
                .hidden-print,
                .hidden-print * {
                    display: none !important;
                }
            }
        </style>
    </head>
    <body onload="window.print()">
        <div class="ticket">
            <p class="centered">TOKO BERKAH</p>
            <p class="centered">
                Transaksi #{{ $transaction->id }}
                <br>{{ $transaction->created_at->isoFormat('DD/MM/Y hh:mm') }}
            </p>

            <table>
                <thead>
                    <tr>
                        <th class="quantity">#</th>
                        <th class="description">Produk</th>
                        <th class="quantity">Q</th>
                        <th class="price right">Harga</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transaction->products as $product)
                    <tr>
                        <td class="quantity centered">{{ $loop->iteration }}</td>
                        <td class="description">{{ $product->nama_barang }}</td>
                        <td class="quantity centered">{{ $product->pivot->jumlah }}</td>
                        <td class="price right">{{ rupiah($product->pivot->harga_satuan) }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <th class="description right" colspan="3">TOTAL</th>
                        <th class="price right">{{ rupiah($transaction->total_harga) }}</th>
                    </tr>
                </tbody>
            </table>
            <p class="centered">Terima kasih</p>
        </div>
    </body>
</html>
