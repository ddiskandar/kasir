@extends('layouts.master')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Daftar Produk</h1>

    @include('flash::message')

    <form method="GET" action="/product">
        <div class="form-row d-flex align-content-end">
            <div class="col-md-7 mb-3">
                <label for="nama_barang">Nama Barang</label>
                <input name="nama_barang" value="{{ request('nama_barang') }}" type="text" class="form-control" id="nama_barang">
            </div>
            <div class="col-md-3 mb-3">
                <label for="harga_satuan">Harga</label>
                <input name="harga_satuan" value="{{ request('harga_satuan') }}" type="number" class="form-control" id="harga_satuan">
            </div>
            <div class="col-md-1 mb-3">
                <label for="operator">Operator</label>
                <select name="operator" class="custom-select" id="operator">
                    <option {{ request('operator') == '=' ? 'selected' : '' }} value='='>=</option>
                    <option {{ request('operator') == '>=' ? 'selected' : '' }} value=">=">>=</option>
                    <option {{ request('operator') == '<=' ? 'selected' : '' }} value="<="><=</option>
                </select>
            </div>
            <div class="col-md-1 mb-3">
                <label for="operator">&nbsp;</label>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar Pengguna</h6>
                    <div class="">
                        <a class="btn btn-success btn-circle btn-sm" href="{{ route('product.create') }}" role="button">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama Barang</th>
                                    <th>Harga Satuan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama Barang</th>
                                    <th>Harga Satuan</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @forelse ($products as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->nama_barang }}</td>
                                    <td>{{ rupiah($product->harga_satuan) }}</td>
                                    <td class="d-flex justify-content-center">
                                        <a class="btn btn-warning btn-circle btn-sm" href="{{ route('product.edit', $product->id) }}" role="button">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <form action="{{ route('product.destroy', $product->id) }}}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger ml-1 btn-circle btn-sm"
                                                onclick="return confirm('Yakin mau dihapus?');"
                                            >
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="4">Tidak ada data yang ditemukan</td>
                                </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('styles')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                columnDefs: [
                    {
                        targets: 2,
                        className: 'text-right'
                    },
                    {
                        targets: 3,
                        className: 'text-center'
                    }
                ],
                "searching": false
                });
        });
    </script>
@endpush
