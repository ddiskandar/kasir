<div>
    @include('flash::message')

    <div class="row">
        <div class="col-lg-8">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col ml-3">
                            <div>
                                Hari/Tanggal :
                            </div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                {{ today()->isoFormat('dddd, DD MMMM gggg') }}
                            </div>
                        </div>
                        <div class="col-auto text-right">
                            <div class="font-weight-bold text-secondary mb-1">
                                Total Harga
                            </div>
                            <div class="h1 mb-0 font-weight-bold text-success">
                                {{ rupiah($totalHarga) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4 ">
                <div class="">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col" class="text-right">Harga</th>
                            <th scope="col" class="text-center">Jumlah</th>
                            <th scope="col" class="text-right">Sub Total</th>
                            <th scope="col" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($transactionProducts as $index => $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item['nama_barang'] }}</td>
                            <td class="text-right">{{ rupiah($item['harga_satuan']) }}</td>
                            <td class="text-center">{{ $item['jumlah'] }}</td>
                            <td class="text-right">{{ rupiah($item['sub_total']) }}</td>
                            <td class="text-center">
                                <button
                                    wire:click="removeProduct({{ $index }})"
                                    type="submit"
                                    class="btn btn-danger ml-1 btn-circle btn-sm">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @empty
                        <tr class="">
                            <td class="text-center py-5" colspan="6">Belum ada barang yang dipilih</td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-flex card-footer justify-content-between">
                    <div>
                        <button wire:click="emptyProduct" class="btn btn-danger">Hapus Semua Item</button>
                    </div>
                    <div>
                        <button wire:click="saveOnly" class="btn btn-primary">Simpan Saja</button>
                        <button wire:click="saveAndPrint" class="btn btn-info">Simpan + Cetak Kuitansi</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card mb-4">
                <div class="card-body">
                    <div wire:ignore class="form-group">
                        <label for="select2">Nama Barang</label>
                        <select id="select2" class=" select2-hidden-accessible " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                            <option value=""></option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->nama_barang }}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="productCount">Jumlah</label>
                        <input type="number" wire:model="productCount" class="form-control text-center">
                    </div>
                    <div>
                        <button wire:click="addProduct" class="btn btn-success btn-block">Tambah</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    @livewireStyles
@endpush

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@livewireScripts
<script>
    $(document).ready(function() {
        $('#select2').select2({
            placeholder: "Pilih atau cari produk",
            allowClear: true
        });
        $('#select2').on('change', function (e) {
            var data = $('#select2').select2("val");
            @this.set('productId', data);
        });
    });
</script>
@endpush
