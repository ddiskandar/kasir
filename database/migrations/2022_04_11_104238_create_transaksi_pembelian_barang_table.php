<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiPembelianBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_pembelian_barang', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaksi_pembelian_id')->constrained('transaksi_pembelian', 'id')->onDelete('cascade');
            $table->foreignId('master_barang_id')->constrained('master_barang', 'id')->onDelete('cascade');
            $table->unsignedInteger('jumlah');
            $table->unsignedInteger('harga_satuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pembelian_barang');
    }
}
