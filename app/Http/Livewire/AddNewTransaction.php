<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class AddNewTransaction extends Component
{
    public $totalHarga = 0;
    public $transactionProducts;
    public $productId;
    public $productCount = 1;
    public $transactionId;

    public function mount()
    {
        $this->transactionProducts = collect();
    }

    public function addProduct()
    {
        if(!isset($this->productId))
        {
            return back();
        }

        $product = Product::find($this->productId);

        $this->transactionProducts->push([
            'master_barang_id' => $product->id,
            'nama_barang' => $product->nama_barang,
            'harga_satuan' => $product->harga_satuan,
            'jumlah' => $this->productCount,
            'sub_total' => $product->harga_satuan * $this->productCount,
        ]);

        $this->productCount = 1;

        $this->setTotalHarga();
    }

    public function removeProduct($index)
    {
        $this->transactionProducts->forget($index);
        $this->setTotalHarga();
    }

    public function setTotalHarga()
    {
        return $this->totalHarga = $this->transactionProducts->sum('sub_total');
    }

    public function emptyProduct()
    {
        $this->transactionProducts = collect();
        $this->setTotalHarga();
    }

    public function saveOnly()
    {
        $this->store();
        flash('Transaksi berhasil disimpan!');
        return redirect()->route('transaction.index');
    }

    public function saveAndPrint()
    {
        $this->store();
        return redirect()->to(route('transaction.print', $this->transactionId));
    }

    public function store()
    {
        if($this->transactionProducts->isEmpty())
        {
            flash('Belum ada produk yang dipilih!')->error()->important();
            return back();
        }

        DB::transaction(function(){
            $transaction = Transaction::create([
                'total_harga' => $this->totalHarga,
            ]);

            foreach($this->transactionProducts as $item)
            {
                $transaction->products()->attach($item['master_barang_id'], [
                    'jumlah' => $item['jumlah'],
                    'harga_satuan' => $item['sub_total'],
                ]);
            }

            $this->transactionId = $transaction->id;
        });

    }

    public function render()
    {
        $products = Product::all();

        return view('livewire.add-new-transaction', [
            'products' => $products,
        ]);
    }
}
