<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class ExportTransactionController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Transaction $transaction)
    {
        $pdf = \PDF::loadView('transaction.export', ['transaction' => $transaction]);

        return $pdf->download('transaksi_' . $transaction->id .'.pdf');
    }
}
