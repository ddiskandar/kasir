<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::query()
            ->when(!auth()->user()->isAdmin(), function($query){
                $query->whereDate('created_at', today());
            })
            ->when(request()->has('tanggal') AND ! empty(request('tanggal')) ?? false, function($query) {
                $query->where('created_at', 'LIKE', '%' . request('tanggal') . '%');
            })
            ->when(request()->has('nama_barang') AND ! empty(request('nama_barang')) ?? false, function($query) {
                $query->whereHas('products', function($query){
                    $query->where('nama_barang', 'LIKE', '%' . request('nama_barang') . '%');
                });
            })
            ->when(request()->has('total_harga') AND ! empty(request('total_harga')) ?? false, function($query) {
                $query->where('total_harga', request('operator'), request('total_harga'));
            })
            ->with('products')
            ->get();

        return view('transaction.index', [
            'transactions' => $transactions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return view('transaction.show', [
            'transaction' => $transaction,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        flash('Transaksi berhasil dihapus!');

        return redirect()->route('transaction.index');
    }
}
