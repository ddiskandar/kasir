<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $transactions = Transaction::query();

        $data['pendapatan_bulan_ini'] = $transactions->whereBetween('created_at', [\Carbon\Carbon::now()->startOfMonth(),\Carbon\Carbon::now()->endOfMonth()])->sum('total_harga');
        $data['jumlah_transaksi_bulan_ini'] = $transactions->whereBetween('created_at', [\Carbon\Carbon::now()->startOfMonth(),\Carbon\Carbon::now()->endOfMonth()])->count();
        $data['pendapatan_hari_ini'] = $transactions->whereDate('created_at', today())->sum('total_harga');
        $data['jumlah_transaksi_hari_ini'] = $transactions->whereDate('created_at', today())->count();

        return view('dashboard',['data' => $data]);
    }
}
