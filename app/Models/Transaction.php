<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transaksi_pembelian';

    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'transaksi_pembelian_barang', 'transaksi_pembelian_id', 'master_barang_id')->withPivot('jumlah', 'harga_satuan');
    }
}
