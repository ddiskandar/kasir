<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function(){
    Route::get('/', App\Http\Controllers\DashboardController::class)->name('dashboard');
    Route::get('/transaction/{transaction}/export', App\Http\Controllers\ExportTransactionController::class)->name('transaction.export');
    Route::get('/transaction/{transaction}/print', App\Http\Controllers\PrintTransactionController::class)->name('transaction.print');
    Route::resource('transaction', App\Http\Controllers\TransactionController::class);
});

Route::middleware(['auth', 'admin'])->group(function(){
    Route::resource('product', App\Http\Controllers\ProductController::class);
    Route::resource('user', App\Http\Controllers\UserController::class);
});

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);
